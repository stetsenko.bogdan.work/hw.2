import express from 'express';
import morgan from 'morgan';
import mongoose from 'mongoose';
import dotenv from 'dotenv';
import cors from 'cors';
//
import notesRouter from './routers/notesRouter.js';
import authRouter from './routers/authRouter.js';
import userRouter from './routers/userRouter.js';

const app = express();
dotenv.config();

const connectToMongo = () => {
  mongoose
    .connect(
      `mongodb+srv://Bohdan:${process.env.PASSWORD}@cluster0.oc3n4.mongodb.net/?retryWrites=true&w=majority`,
    )
    .then(() => {
      console.log('Connected to MongoDB!');
    });
};

function errorHandler(err, req, res, next) {
  const status = err.status || 500;
  const message = err.message || 'String';
  res.status(status).json({message});
}

app.use(express.json());
app.use(morgan('tiny'));
app.use(cors());
app.use('/api/users/me', userRouter);
app.use('/api/notes', notesRouter);
app.use('/api/auth', authRouter);

app.use(errorHandler);

const start = async () => {
  try {
    app.listen(8080, (err) => {
      connectToMongo();
      console.log('Server has been started!');
    });
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();
