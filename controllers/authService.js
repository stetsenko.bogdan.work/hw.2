import User from '../models/Users.js';
import Credential from '../models/Credentials.js';

import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import {createError} from '../createError.js';
export const registerUser = async (req, res, next) => {
  const {username, password} = req.body;
  if (!username || !password) {
    next(createError(400));
  }
  const user = new User({
    username,
    createdDate: new Date().toISOString(),
  });
  const credential = new Credential({
    username,
    password: await bcrypt.hash(String(password), 10),
  });

  user
    .save()
    .then(() =>
      credential
        .save()
        .then(() => {
          res.json({
            message: 'Success',
          });
        })
        .catch(() => {
          next(createError(500));
        }),
    )
    .catch((err) => {
      next(createError(500));
    });
};

export const loginUser = async (req, res, next) => {
  try {
    const user = await User.findOne({
      username: req.body.username,
    });
    const userInfo = await Credential.findOne({
      username: req.body.username,
    });
    if (user) {
      const isCorrect = await bcrypt.compare(
        String(req.body.password),
        String(userInfo.password),
      );
      if (isCorrect) {
        const jwtToken = jwt.sign({id: user._id}, process.env.KEY);
        res.json({message: 'Success', jwt_token: jwtToken});
      } else {
        next(createError(400));
      }
    } else {
      next(createError(400));
    }
  } catch (err) {
    next(createError(500));
  }
};
