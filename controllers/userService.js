import User from '../models/Users.js';
import {createError} from '../createError.js';
import bcrypt from 'bcryptjs';
import Credential from '../models/Credentials.js';
export const getUserInfo = async (req, res, next) => {
  const user = await User.findById(req.user.userId);
  if (!user) {
    next(createError(400));
  }
  const {_id, username, createdDate} = user;
  res.json({
    user: {
      _id,
      username,
      createdDate,
    },
  });
};

export const deleteUser = async (req, res, next) => {
  try {
    const user = await User.findById(req.user.userId);
    await Credential.findOneAndDelete({username: user.username}).catch(() => {
      next(createError(400));
    });
    return await User.findByIdAndDelete(req.user.userId)
      .then(() => {
        res.json({message: 'Success'});
      })
      .catch(() => {
        next(createError(400));
      });
  } catch (error) {
    next(createError(500));
  }
};

export const updatePassword = async (req, res, next) => {
  try {
    const user = await User.findById(req.user.userId);
    const userInfo = await Credential.findOne({username: user.username});
    userInfo.password = await bcrypt.hash(String(req.body.newPassword), 10);
    return userInfo
      .save()
      .then(() => res.json({message: 'Success'}))
      .catch(() => {
        next(createError(500));
      });
  } catch (error) {
    next(createError(500));
  }
};
