import Note from '../models/Notes.js';
import {createError} from '../createError.js';

export function createNote(req, res, next) {
  const {text} = req.body;
  if (!text) {
    next(createError(400));
  }
  const note = new Note({
    userId: req.user.userId,
    text,
    createdDate: new Date().toISOString(),
  });
  note
    .save()
    .then(() => {
      res.json({
        message: 'Success',
      });
    })
    .catch(() => {
      next(createError(500));
    });
}

export const getNotes = async (req, res, next) => {
  const note = await Note.find({userId: req.user.userId});
  const offset = +req.query.offset;
  const limit = +req.query.limit;
  if (!note) {
    next(createError(400));
  }
  res.json({
    offset,
    limit,
    count: note.length,
    notes: (function() {
      if (offset && limit) {
        return note.slice(offset - 1, offset + limit - 1);
      } else if (offset) {
        if (offset >= note.length) {
          return [];
        } else {
          return note.slice(offset - 1);
        }
      } else if (limit) {
        return note.slice(0, limit);
      } else {
        return note;
      }
    })(),
  });
};

export const getNoteById = (req, res, next) => {
  try {
    Note.findById(req.params.id)
      .then((data) => {
        console.log(data);
        const {_id, userId, completed, text, createdDate} = data;
        res.json({
          note: {
            _id,
            userId,
            completed,
            text,
            createdDate,
          },
        });
      })
      .catch(() => {
        next(createError(400));
      });
  } catch (error) {
    next(createError(500));
  }
};
export const updateNoteById = async (req, res, next) => {
  if (!req.body) {
    next(createError(400));
  }
  const note = await Note.findByIdAndUpdate(
    req.params.id,
    {$set: req.body},
    {new: true},
  );

  note
    .save()
    .then(() =>
      res.json({
        message: 'Success',
      }),
    )
    .catch(() => {
      next(createError(500));
    });
};

export const checkNote = async (req, res, next) => {
  const note = await Note.findById(req.params.id);
  if (!note) {
    next(createError(400));
  }
  note.completed = !note.completed;
  return note
    .save()
    .then(() => res.json({message: 'Success'}))
    .catch(() => {
      next(createError(500));
    });
};
export const deleteNote = async (req, res, next) => {
  if (!req.params.id) {
    next(400);
  }
  await Note.findByIdAndDelete(req.params.id)
    .then(() => res.json({message: 'Success'}))
    .catch(() => {
      next(createError(500));
    });
};
