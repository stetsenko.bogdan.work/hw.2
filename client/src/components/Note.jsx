import React, { useState } from 'react';
import AdjustOutlinedIcon from '@mui/icons-material/AdjustOutlined';
import './Note.scss';
import CancelIcon from '@mui/icons-material/Cancel';
import ReplayIcon from '@mui/icons-material/Replay';
const Note = ({ item, toggleComplete, deleteUserNote, updateUserNote }) => {
  const [noteText, setNoteText] = useState(item.text);
  return (
    <li className='user-note'>
      <div className='note-info'>
        <span className={item.completed ? 'true' : 'false'}>
          <AdjustOutlinedIcon />
        </span>
        <input
          className='note-text'
          value={noteText}
          type='text'
          onChange={(e) => setNoteText(e.target.value)}
        ></input>
        <button
          className='note-btn update'
          onClick={() => updateUserNote(item._id, noteText)}
        >
          <ReplayIcon />
        </button>
      </div>
      <div className='note-func'>
        <button
          onClick={() => toggleComplete(item._id)}
          className={
            item.completed ? 'note-btn uncomplete' : 'note-btn complete'
          }
        >
          {item.completed ? 'Uncomplete' : 'Complete'}
        </button>
        <button
          className='note-btn delete'
          onClick={() => deleteUserNote(item._id)}
        >
          <CancelIcon />
        </button>
      </div>
    </li>
  );
};

export default Note;
