import React, { useState } from 'react';
import './Navbar.scss';
import AccountCircleOutlinedIcon from '@mui/icons-material/AccountCircleOutlined';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { logout } from '../redux/userSlice';
import useService from '../service/useService';
import { logoutToken } from '../redux/tokenSlice';
import ExitToAppOutlinedIcon from '@mui/icons-material/ExitToAppOutlined';
import PersonRemoveIcon from '@mui/icons-material/PersonRemove';
import CheckOutlinedIcon from '@mui/icons-material/CheckOutlined';
const Navbar = () => {
  const { currentUser } = useSelector((state) => state.user);
  const { currentToken } = useSelector((state) => state.token);
  const [changePass, setChangePass] = useState(false);
  const [password, setPassword] = useState('');
  const { deleteUser, updatePassword } = useService();
  const dispatch = useDispatch();
  const logOut = () => {
    try {
      dispatch(logout());
      dispatch(logoutToken());
    } catch (error) {
      console.log(error);
    }
  };
  const deleteCurrentUser = async () => {
    try {
      const result = await deleteUser(currentToken);
      dispatch(logout());
      dispatch(logoutToken());
      return result;
    } catch (error) {
      console.log(error);
    }
  };
  const updateUserPassword = async () => {
    try {
      const result = await updatePassword(
        { newPassword: password },
        currentToken
      );
      dispatch(logout());
      dispatch(logoutToken());
      setChangePass(!changePass);
      return result;
    } catch (error) {
      console.log(error);
    }
  };
  console.log();
  return (
    <nav className='nav'>
      <div className='nav-container'>
        <Link to='/auth' style={{ textDecoration: 'none' }}>
          {!currentUser ? (
            <button className='nav-btn'>
              <AccountCircleOutlinedIcon />
              Sign In / Sign Up
            </button>
          ) : null}
        </Link>
        <div className='user'>
          {currentUser ? (
            <>
              <div
                className='user-change'
                style={{ opacity: changePass ? '1' : '0' }}
              >
                <input
                  className='auth-content__input'
                  type='text'
                  placeholder='Change password'
                  required
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                ></input>
                <button
                  className='nav-btn logout'
                  title='Logout'
                  onClick={updateUserPassword}
                >
                  <CheckOutlinedIcon />
                </button>
              </div>
              <div className='user-info'>
                <button
                  className='nav-btn logout'
                  title='Logout'
                  onClick={logOut}
                >
                  <ExitToAppOutlinedIcon />
                </button>
                <button
                  className='nav-btn logout'
                  title='Delete user'
                  onClick={deleteCurrentUser}
                >
                  <PersonRemoveIcon />
                </button>
                <img
                  alt='img'
                  className='user-img'
                  src='https://www.pavilionweb.com/wp-content/uploads/2017/03/man-300x300.png'
                ></img>
                <div className='user-name'>{currentUser.username}</div>
                <button
                  className='nav-btn'
                  onClick={() => setChangePass(!changePass)}
                >
                  Change password
                </button>
              </div>
            </>
          ) : null}
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
