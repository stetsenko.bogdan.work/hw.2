import { React, useEffect, useRef, useState } from 'react';
import useService from '../service/useService';
import { useSelector } from 'react-redux';
import './NotesList.scss';
import Note from './Note';
const NotesList = () => {
  const [userNotes, setUserNotes] = useState([]);
  const [noteText, setNoteText] = useState('');
  const { currentToken } = useSelector((state) => state.token);
  const updateModal = useRef();
  const {
    getUserNotes,
    createNote,
    changeCompletedById,
    deleteNote,
    updateNote,
  } = useService();
  useEffect(() => {
    const getNotes = async () => {
      const result = await getUserNotes(currentToken);
      setUserNotes(result.data.notes);
    };
    getNotes();
  }, []);
  const createUserNote = async () => {
    const result = await createNote(noteText, currentToken);
    if (result.data.message === 'Success') {
      const result = await getUserNotes(currentToken);
      setUserNotes(result.data.notes);
      setNoteText('');
    }
  };
  const toggleComplete = async (id) => {
    const result = await changeCompletedById(id, currentToken);
    if (result.data.message === 'Success') {
      const newList = userNotes.map((elem) => {
        if (elem._id === id) {
          elem.completed = !elem.completed;
          return elem;
        } else {
          return elem;
        }
      });
      setUserNotes(newList);
    }
  };
  const deleteUserNote = async (id) => {
    const result = await deleteNote(id, currentToken);
    if (result.data.message === 'Success') {
      const newList = userNotes.filter((elem) => {
        return elem._id !== id;
      });
      setUserNotes(newList);
    }
  };
  const updateUserNote = async (id, text) => {
    const result = await updateNote(id, text, currentToken);
    if (result.data.message === 'Success') {
      const newList = userNotes.map((elem) => {
        if (elem._id === id) {
          setTimeout(() => {
            elem.text = text;
            updateModal.current.className = 'note-modal updated';
            setTimeout(() => {
              updateModal.current.className = 'note-modal';
            }, 3000);
          }, 0);

          elem.text = text;
          return elem;
        } else {
          return elem;
        }
      });
      setUserNotes(newList);
    }
  };
  const items = userNotes.map((item) => {
    return (
      <Note
        key={item._id}
        item={item}
        toggleComplete={toggleComplete}
        deleteUserNote={deleteUserNote}
        updateUserNote={updateUserNote}
      />
    );
  });

  return (
    <div className='user-wrapper'>
      <div className='note-modal' ref={updateModal}>
        Note updated
      </div>
      <h2 className='user-wrapper__title'>User Notes</h2>
      <div className='note-field'>
        <input
          id='note'
          type='text'
          className='note-input'
          required
          placeholder='Note text'
          value={noteText}
          onChange={(e) => setNoteText(e.target.value)}
        />
        <button className='note-btn create' onClick={createUserNote}>
          Create Note
        </button>
      </div>
      <ul className='user-notes'>{items}</ul>
    </div>
  );
};

export default NotesList;
