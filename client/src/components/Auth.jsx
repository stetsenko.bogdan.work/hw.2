import RemoveRedEyeOutlinedIcon from '@mui/icons-material/RemoveRedEyeOutlined';
import React, { useRef, useState } from 'react';
import useService from '../service/useService.js';
import { useDispatch } from 'react-redux';
import { loginStart, loginSuccess, loginFailure } from '../redux/userSlice';
import { tokenSuccess } from '../redux/tokenSlice';

import './Auth.scss';
const Auth = () => {
  const [signIn, setSignIn] = useState(false);
  const passwordToggleReg = useRef(null);
  const passwordToggleLog = useRef(null);

  const dispatch = useDispatch();
  const { signUpUser, signInUser, userInfo, getUserNotes } = useService();
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const signupUser = async (e) => {
    e.preventDefault();
    try {
      const result = await signUpUser({ username, password });
      if (result.data.message === 'Success') {
        setSignIn(!signIn);
        setUsername('');
        setPassword('');
      }
    } catch (error) {}
  };
  const signinUser = async (e) => {
    e.preventDefault();
    dispatch(loginStart());
    try {
      const result = await signInUser({ username, password });
      if (result.data.message === 'Success') {
        dispatch(tokenSuccess(result.data.jwt_token));
        const user = await userInfo(result.data.jwt_token);
        await getUserNotes(result.data.jwt_token);
        dispatch(loginSuccess(user.data.user));
      }
    } catch (error) {
      dispatch(loginFailure());
    }
  };
  return (
    <>
      <div className='auth-wrapper'></div>
      <div className={signIn ? 'auth-content register' : 'auth-content'}>
        <div className='auth-content__field'>
          <input
            id='usernamereg'
            type='text'
            className='auth-content__input username'
            required
            placeholder=' '
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
          <label htmlFor='usernamereg' className='auth-content__label'>
            Username
          </label>
        </div>
        <div className='auth-content__field'>
          <input
            id='passwordreg'
            type='password'
            className='auth-content__input password'
            required
            placeholder=' '
            ref={passwordToggleReg}
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
          <label htmlFor='passwordreg' className='auth-content__label'>
            Password
          </label>
          <span
            className='toggle-password'
            onClick={() => {
              passwordToggleReg.current.type === 'password'
                ? (passwordToggleReg.current.type = 'text')
                : (passwordToggleReg.current.type = 'password');
            }}
          >
            <RemoveRedEyeOutlinedIcon />
          </span>
        </div>
        <div className='auth-content__field'>
          <button className='auth-content__btn' onClick={signupUser}>
            Sign Up
          </button>
        </div>
        <div className='auth-content__field'>
          <div
            className={signIn ? 'auth-login hide' : 'auth-login'}
            onClick={() => {
              setSignIn(!signIn);
            }}
          >
            Login
          </div>
        </div>
      </div>
      <div className={!signIn ? 'auth-content login' : 'auth-content'}>
        <div className='auth-content__field'>
          <input
            id='usernamelog'
            type='text'
            className='auth-content__input username'
            required
            placeholder=' '
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
          <label htmlFor='usernamelog' className='auth-content__label'>
            Username
          </label>
        </div>
        <div className='auth-content__field'>
          <input
            id='passwordlog'
            type='password'
            className='auth-content__input password'
            required
            placeholder=' '
            ref={passwordToggleLog}
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
          <label htmlFor='passwordlog' className='auth-content__label'>
            Password
          </label>
          <span
            className='toggle-password'
            onClick={() => {
              passwordToggleLog.current.type === 'password'
                ? (passwordToggleLog.current.type = 'text')
                : (passwordToggleLog.current.type = 'password');
            }}
          >
            <RemoveRedEyeOutlinedIcon />
          </span>
        </div>
        <div className='auth-content__field'>
          <button className='auth-content__btn' onClick={signinUser}>
            Sign In
          </button>
        </div>
        <div className='auth-content__field'>
          <div
            className={!signIn ? 'auth-login hide' : 'auth-login'}
            onClick={() => {
              setSignIn(!signIn);
            }}
          >
            Register
          </div>
        </div>
      </div>
    </>
  );
};

export default Auth;
