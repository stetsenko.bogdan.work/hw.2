import Auth from './components/Auth';
import './App.scss';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Navbar from './components/Navbar';
import { useSelector, useDispatch } from 'react-redux';
import NotesList from './components/NotesList';
function App() {
  const { currentUser } = useSelector((state) => state.user);
  return (
    <div className='App'>
      <BrowserRouter>
        <Navbar />
        <Routes>
          <Route path='/'>
            {!currentUser ? (
              <Route path='auth' element={<Auth />}></Route>
            ) : null}
            <Route path='auth' element={<NotesList />}></Route>
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
