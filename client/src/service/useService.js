import axios from 'axios';
const useService = () => {
  const _requestBase = 'http://localhost:8080/api';
  const signUpUser = async (body) => {
    const result = await axios.post(`${_requestBase}/auth/register`, body);
    return result;
  };
  const signInUser = async (body) => {
    const result = await axios.post(`${_requestBase}/auth/login`, body);
    return result;
  };
  const userInfo = async (token) => {
    const result = await axios.get(`${_requestBase}/users/me`, {
      headers: { Authorization: 'Bearer ' + token },
    });
    return result;
  };
  const getUserNotes = async (token) => {
    const result = await axios.get(`${_requestBase}/notes`, {
      headers: { Authorization: 'Bearer ' + token },
    });
    return result;
  };
  const createNote = async (text, token) => {
    const result = await axios.post(
      `${_requestBase}/notes`,
      { text },
      {
        headers: { Authorization: 'Bearer ' + token },
      }
    );
    return result;
  };
  const changeCompletedById = async (id, token) => {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    const result = await axios.patch(`${_requestBase}/notes/${id}`);
    return result;
  };
  const deleteNote = async (id, token) => {
    const result = await axios.delete(`${_requestBase}/notes/${id}`, {
      headers: { Authorization: 'Bearer ' + token },
    });
    return result;
  };
  const updateNote = async (id, text, token) => {
    const result = await axios.put(
      `${_requestBase}/notes/${id}`,
      { text },
      {
        headers: { Authorization: 'Bearer ' + token },
      }
    );
    return result;
  };
  const deleteUser = async (token) => {
    const result = await axios.delete(`${_requestBase}/users/me`, {
      headers: { Authorization: 'Bearer ' + token },
    });
    return result;
  };
  const updatePassword = async (body, token) => {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    const result = await axios.patch(`${_requestBase}/users/me`, body);
    return result;
  };
  return {
    userInfo,
    signInUser,
    signUpUser,
    getUserNotes,
    createNote,
    changeCompletedById,
    deleteNote,
    updateNote,
    deleteUser,
    updatePassword,
  };
};

export default useService;
