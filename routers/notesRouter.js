import express from 'express';
const router = express.Router();
import {
  createNote,
  getNotes,
  getNoteById,
  updateNoteById,
  checkNote,
  deleteNote,
} from '../controllers/notesService.js';
import authMiddleware from '../middleware/authMiddleware.js';
router.post('/', authMiddleware, createNote);

router.get('/', authMiddleware, getNotes);

router.get('/:id', authMiddleware, getNoteById);

router.put('/:id', updateNoteById);

router.patch('/:id', authMiddleware, checkNote);

router.delete('/:id', authMiddleware, deleteNote);

export default router;
