import express from 'express';
const router = express.Router();
import {
  getUserInfo,
  deleteUser,
  updatePassword,
} from '../controllers/userService.js';
import authMiddleware from '../middleware/authMiddleware.js';
router.get('/', authMiddleware, getUserInfo);
router.delete('/', authMiddleware, deleteUser);
router.patch('/', authMiddleware, updatePassword);

export default router;
