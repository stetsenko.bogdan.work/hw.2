import mongoose from 'mongoose';

const UserSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
    unique: true,
  },
  createdDate: {
    type: String,
    default: '',
  },
});

export default mongoose.model('UsersHW2', UserSchema);
