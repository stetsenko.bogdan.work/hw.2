import mongoose from 'mongoose';

const Note = new mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  completed: {
    type: Boolean,
    default: false,
  },
  text: {
    type: String,
    default: '',
  },
  createdDate: {
    type: String,
    default: '',
  },
});

export default mongoose.model('Note', Note);
